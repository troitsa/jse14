package ru.vlasova.iteco.taskmanager.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.vlasova.iteco.taskmanager.api.service.ServiceLocator;
import ru.vlasova.iteco.taskmanager.dto.ProjectDTO;
import ru.vlasova.iteco.taskmanager.dto.TaskDTO;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.entity.User;

import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@WebService(endpointInterface = "ru.vlasova.iteco.taskmanager.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    public ProjectDTO insertProject(@Nullable final String token,
                                    @Nullable final String userId,
                                    @Nullable final String name,
                                    @Nullable final String description,
                                    @Nullable final String dateStart,
                                    @Nullable final String dateFinish) throws Exception {
        validateSession(token);
        @Nullable final User user = serviceLocator.getUserService().findOne(userId);
        return toProjectDTO(serviceLocator.getProjectService().insert(user, name, description, dateStart, dateFinish));
    }

    @Override
    @Nullable
    public List<ProjectDTO> findAllProjects(@Nullable final String token) throws Exception {
        validateSession(token);
        return serviceLocator.getProjectService()
                .findAll()
                .stream()
                .map(this::toProjectDTO)
                .collect(Collectors.toList());
    }

    @Override
    @Nullable
    public List<ProjectDTO> findAllProjectsByUserId(@Nullable final String token,
                                                    @NotNull final String userId) throws Exception {
        validateSession(token);
        return serviceLocator.getProjectService().findAll(userId).stream()
                .map(this::toProjectDTO)
                .collect(Collectors.toList());
    }

    @Override
    public @Nullable ProjectDTO findOneProject(@Nullable final String token,
                                               @NotNull final String id) throws Exception {
        validateSession(token);
        return toProjectDTO(serviceLocator.getProjectService().findOne(id));
    }

    @Override
    public @Nullable ProjectDTO findOneProjectByUserId(@Nullable final String token,
                                                       @NotNull final String userId,
                                                       @NotNull final String id) throws Exception {
        validateSession(token);
        return toProjectDTO(serviceLocator.getProjectService().findOneByUserId(userId, id));
    }

    @Override
    public void persistProject(@Nullable final String token,
                               @NotNull final ProjectDTO projectDTO) throws Exception {
        validateSession(token);
        serviceLocator.getProjectService().persist(toProject(projectDTO));
    }

    @Override
    public void mergeProject(@Nullable final String token,
                             @NotNull final ProjectDTO projectDTO) throws Exception {
        validateSession(token);
        serviceLocator.getProjectService().merge(toProject(projectDTO));
    }

    @Override
    public void removeProjectByUserId(@Nullable final String token,
                                      @NotNull final String userId,
                                      @NotNull final String id) throws Exception {
        validateSession(token);
        serviceLocator.getProjectService().remove(userId, id);
    }

    @Override
    public void removeProjectById(@Nullable final String token,
                           @NotNull String id) throws Exception {
        validateSession(token);
        serviceLocator.getProjectService().removeById(id);
    }

    @Override
    public void removeAllProjects(@Nullable final String token) throws Exception {
        validateSession(token);
        serviceLocator.getProjectService().removeAll();
    }

    @Override
    public void removeAllProjectByUserId(@Nullable final String token,
                                         @NotNull final String userId) throws Exception {
        validateSession(token);
        serviceLocator.getProjectService().removeAll(userId);
    }

    @Override
    public @Nullable ProjectDTO getProjectByIndex(@Nullable final String token,
                                                  @Nullable final String userId,
                                                  int index) throws Exception {
        validateSession(token);
        return toProjectDTO(serviceLocator.getProjectService().getProjectByIndex(userId, index));
    }

    @Override
    public @Nullable List<TaskDTO> getTasksByProjectIndex(@Nullable final String token,
                                                          @Nullable final String userId,
                                                          int projectIndex) throws Exception {
        validateSession(token);
        return serviceLocator.getProjectService()
                .getTasksByProjectIndex(userId, projectIndex)
                .stream()
                .map(this::toTaskDTO)
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public List<ProjectDTO> searchProject(@Nullable final String token,
                                                    @Nullable final String userId,
                                                    @Nullable final String searchString) throws Exception {
        validateSession(token);
        return serviceLocator.getProjectService()
                .search(userId, searchString)
                .stream()
                .map(this::toProjectDTO)
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public List<ProjectDTO> sortProject(@Nullable final String userId,
                                        @Nullable final String sortMode) {
        return serviceLocator.getProjectService()
                .sortProject(userId, sortMode)
                .stream()
                .map(this::toProjectDTO)
                .collect(Collectors.toList());
    }

    @Nullable
    private Project toProject(@Nullable final ProjectDTO projectDTO) {
        if (projectDTO == null) return null;
        @Nullable final Project project = new Project();
        project.setId(projectDTO.getId());
        User user = serviceLocator.getUserService().findOne(projectDTO.getUserId());
        if (user == null) return null;
        project.setUser(user);
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setDateCreate(projectDTO.getDateCreate());
        project.setDateStart(projectDTO.getDateStart());
        project.setDateFinish(projectDTO.getDateFinish());
        project.setStatus(projectDTO.getStatus());
        return project;
    }

    @Nullable
    private ProjectDTO toProjectDTO(@Nullable final Project project) {
        if (project == null) return null;
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setUserId(project.getUser().getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setDateCreate(project.getDateCreate());
        projectDTO.setDateStart(project.getDateStart());
        projectDTO.setDateFinish(project.getDateFinish());
        projectDTO.setStatus(project.getStatus());
        return projectDTO;
    }

    @Nullable
    private TaskDTO toTaskDTO(@Nullable final Task task) {
        if (task == null) return null;
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setUserId(task.getUser().getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setDateCreate(task.getDateCreate());
        taskDTO.setDateStart(task.getDateStart());
        taskDTO.setDateFinish(task.getDateFinish());
        taskDTO.setStatus(task.getStatus());
        return taskDTO;
    }
}