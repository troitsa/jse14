package ru.vlasova.iteco.taskmanager.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.vlasova.iteco.taskmanager.api.service.ServiceLocator;
import ru.vlasova.iteco.taskmanager.dto.UserDTO;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@WebService(endpointInterface = "ru.vlasova.iteco.taskmanager.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @Nullable UserDTO insertUser(@Nullable final String login,
                                        @Nullable final String password) {
        return toUserDTO(serviceLocator.getUserService().insert(login, password));
    }

    @Override
    public @Nullable UserDTO doLogin(@Nullable final String login,
                                  @Nullable final String password) throws Exception {
        return toUserDTO(serviceLocator.getUserService().doLogin(login, password));
    }

    @Override
    public @Nullable String checkUser(@Nullable final String token,
                                      @Nullable final String login) throws Exception {
        validateSession(token);
        return serviceLocator.getUserService().checkUser(login);
    }

    @Override
    public void editUser(@Nullable final String token,
                         @Nullable final UserDTO userDTO,
                         @Nullable final String login,
                         @Nullable final String password) throws Exception {
        validateSession(token);
        serviceLocator.getUserService().edit(toUser(userDTO), login, password);
    }

    @Override
    public @Nullable List<UserDTO> findAllUsers(@Nullable final String token) throws Exception {
        validateSession(token);
        return serviceLocator.getUserService()
                .findAll().stream()
                .map(this::toUserDTO)
                .collect(Collectors.toList());
    }

    @Override
    public @Nullable UserDTO findUserBySession(@Nullable final String token,
                                      @Nullable final String id) throws Exception {
        validateSession(token);
        return toUserDTO(serviceLocator.getUserService().findOne(id));
    }

    @Override
    public @Nullable UserDTO findUser(@Nullable final String id) throws Exception {
        return toUserDTO(serviceLocator.getUserService().findOne(id));
    }

    @Override
    public void persistUser(@Nullable final UserDTO userDTO) throws Exception {
        serviceLocator.getUserService().persist(toUser(userDTO));
    }

    @Override
    public void mergeUser(@Nullable final String token,
                          @Nullable final UserDTO userDTO) throws Exception {
        validateSession(token);
        serviceLocator.getUserService().merge(toUser(userDTO));
    }

    @Override
    public void removeUser(@Nullable final String token,
                           @Nullable final String id) {
        serviceLocator.getUserService().remove(id);
    }

    @Override
    public void removeAllUsers(@Nullable final String token) throws Exception {
        validateSession(token);
        serviceLocator.getUserService().removeAll();
    }

    @Override
    public Boolean checkRole(@Nullable String userId,
                             @NotNull List<Role> roles) {
        return serviceLocator.getUserService().checkRole(userId, roles);
    }

    @Nullable
    private UserDTO toUserDTO(@Nullable final User user) {
        if (user == null) return null;
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setPwd(user.getPwd());
        userDTO.setRole(user.getRole());
        return userDTO;
    }

    @Nullable
    private User toUser(@Nullable final UserDTO userDTO) {
        if (userDTO == null) return null;
        @Nullable final User user = new User();
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setPwd(userDTO.getPwd());
        user.setRole(userDTO.getRole());
        return user;
    }

}
