package ru.vlasova.iteco.taskmanager.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.ServiceLocator;

@NoArgsConstructor
public class AbstractEndpoint {

    @Nullable
    @Getter
    protected ServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void validateSession(@Nullable final String token) throws Exception {
        if (token == null) throw new Exception("You are not authorized");
        serviceLocator.getSessionService().validate(token);
    }

}
