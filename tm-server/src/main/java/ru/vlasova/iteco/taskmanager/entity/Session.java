package ru.vlasova.iteco.taskmanager.entity;

import com.fasterxml.jackson.annotation.JsonFilter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@XmlType
@NoArgsConstructor
@Entity(name = "app_session")
public class Session extends AbstractEntity {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    @ManyToOne
    @JsonFilter("idFilter")
    private User user;

    @Nullable
    @Enumerated(value = EnumType.STRING)
    private Role role;

    @Nullable
    private String signature;

    @NotNull
    @Column(columnDefinition = "BIGINT", updatable = false)
    private long createDate = new Date().getTime();

}
