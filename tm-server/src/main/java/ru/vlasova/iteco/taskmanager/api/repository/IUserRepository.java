package ru.vlasova.iteco.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.User;

import java.util.List;

public interface IUserRepository {

    @Nullable
    String checkUser(@NotNull final String login);

    @NotNull
    List<User> findAll();

    @Nullable
    User findOne(@NotNull  final String id);

    void persist(@NotNull final User obj);

    void merge(@NotNull final User obj);

    void remove(@NotNull final String id);

    void removeAll();

}
