package ru.vlasova.iteco.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.IUserRepository;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.api.service.ServiceLocator;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.AccessDeniedException;
import ru.vlasova.iteco.taskmanager.repository.UserRepository;
import ru.vlasova.iteco.taskmanager.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserService implements IUserService {

    @NotNull
    private final ServiceLocator serviceLocator;

    public UserService(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void edit(@Nullable final User user,
                     @Nullable final String login,
                     @Nullable final String password) {
        if (!isValid(login, password) || user == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        user.setLogin(login);
        user.setPwd(HashUtil.MD5(password));
        repository.merge(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    @Nullable
    public User insert(@Nullable final String login, @Nullable final String password) {
        final boolean checkGeneral = isValid(login, password);
        if (!checkGeneral) return null;
        return new User(login, password);
    }

    @Override
    @Nullable
    public User doLogin(final String login, final String password) throws Exception {
        @Nullable final String id = checkUser(login);
        if (id == null) return null;
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        @Nullable final User user = repository.findOne(id);
        entityManager.getTransaction().commit();
        entityManager.close();
        if (user == null) return null;
        @Nullable final String pwd = user.getPwd();
        if (pwd == null) return null;
        if (pwd.equals(HashUtil.MD5(password))) {
            return user;
        } else {
            throw new AccessDeniedException("User or password incorrect");
        }
    }

    @Override
    @Nullable
    public String checkUser(final String login) {
        if (!isValid(login)) return null;
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        @Nullable String id = repository.checkUser(login);
        entityManager.getTransaction().commit();
        entityManager.close();
        return id;
    }

    @Override
    public boolean checkRole(@Nullable final String userId, @Nullable final List<Role> roles) {
        if (userId == null || roles == null) return false;
        @Nullable User user = findOne(userId);
        if (user == null) return false;
        for (@Nullable Role role : roles) {
            if (user.getRole().equals(role)) return true;
        }
        return false;
    }

    @Override
    public void merge(@Nullable final User user) {
        if (user == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        repository.merge(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void persist(@Nullable final User user)  {
        if (user == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        repository.persist(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    @NotNull
    public List<User> findAll() {
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        @NotNull final List<User> users = repository.findAll();
        entityManager.getTransaction().commit();
        entityManager.close();
        return users;
    }

    @Override
    @Nullable
    public User findOne(@Nullable final String id) {
        if (id == null) return null;
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        @Nullable final User user = repository.findOne(id);
        entityManager.getTransaction().commit();
        entityManager.close();
        return user;
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        repository.remove(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        repository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}
